SHELL=/bin/bash

LC_ALL=en_US.UTF-8
LANG=en_US.UTF-8

ANSIBLE_SSH_ARGS='-F ssh.config'
ANSIBLE_ENABLE_TASK_DEBUGGER=True
ANSIBLE_LIBRARY=${CURDIR}/module_utils
# Simply by being mentioned as a target,
# this tells make to export all variables to child processes by default.
.EXPORT_ALL_VARIABLES:

# Sets the default goal to be used if no targets were specified on
# the command line (see Arguments to Specify the Goals). The .DEFAULT_GOAL variable allows you
# to discover the current default goal, restart the default goal selection algorithm
# by clearing its value, or to explicitly set the default goal.
.DEFAULT_GOAL := help

.ONESHELL:
up:
	vagrant up --provision --parallel

.ONESHELL:
down:
	vagrant halt --force

.ONESHELL:
destroy:
	vagrant destroy -f

.ONESHELL:
reload:
	vagrant reload -f

.ONESHELL:
ssh-config:
	vagrant ssh-config > ssh.config


.ONESHELL:
deploy: ssh-config
	ansible-playbook --inventory-file hosts.yml --forks 10 --flush-cache --verbose site.yml
